Catering
======================

Dominio del problema
--------------------

A partir de la pandemia el comedor de de FIUBA decidió diversificar su negocio y comenzar a ofrecer servicios de catering y para ello requiere una aplicacion (web api) que permita hacer presupuestos.
Los servicios de catering ofrecidos se ajustan a distintos tipos de ocasiones: eventos empresariales, casamientos y fiestas de 15.


* Eventos empresariales: son para un mínimo de 20 comensales y se realizan en las instalaciones que la empresa designe. Costo base 200.
* Casamientos: en general se realizan en el lugar designado por quien contrata, pero existe también la posiblidad de que el servicio incluya la contración del salón. Tienen una restricción de máximo de comensales de 300. En el caso de contratar el salón hay una capacidad máxima de 150 comensales. Costo base 150
* Fiestas de 15: al igual que los casamientos pueden incluir o no la provisión del salón. Costo base 100

En cada presupuesto debe indicarse:
* cuit del contratante
* el tipo de catering
* el nivel de servicio (normal,superior o premium)
* fecha tentativa del evento
* la cantidad de comensales
* el menu (veggie, carnie, mixto)

El presupuesto se calcula como COSTO_BASE * CANTIDAD_DE_COMENZALES + CANTIDAD_DE_MOZOS * COSTO_MOZO + MENU * CANTIDAD_DE_COMENZALES + COSTO_SALON (si es contratado)

Donde la CANTIDAD_DE_MOZOS es:
* 1 cada 30 comenzales para servicio normal
* 1 cada 25 comenzales para servicio superior
* 1 cada 10 comenzales para servicio premium

El costo de cada mozo es de 200, salvo el mozo especialista en cortado de torta cuyo costo es de 150.

El costo del salón, en caso de ser contratado es de 1000.

Donde la MENU el costo del menu es:
* veggie 50 
* carnie 60
* mixto 55

Si el evento es por sábado, domingo o feriado, tiene un costo adicional de 10%.
Los casamientos y fiestas de 15 no pueden ser contratados por empresas.
Nota: el cuit de las empresas comienza con 30 mientras que el de las personas físicas comienza con 20 o 27.


Funcionalidades a desarrollar
------------------------------

### crear presupuesto

````
Request:
POST /presupuestos
body
{ cuit:2029111222, tipo_evento: casamiento, nivel_servicio: normal, fecha: 2021-05-10, salon:false }

Response: 
201 
{
"id": "1"
"cuit":"2029111222", 
"tipo_evento": "casamiento",
"nivel_servicio": "normal", 
"fecha": "2021-05-10",
"salon": false
}

````

### agregar comenzales al presupuesto
````
Request:
PATCH /presupuestos/{id}/
body
{ "cantidad_de_comensales": 10, "menu":"mixto" }

Response: 
201 
{
    datos prespuesto
}

````

### consultar presuepuesto

```
Request:
GET /presupuestos/{id}

Response: 
200 
{ 
    "detalles del prespuesto": []
    "importe": 100.0
}

````

### Reset
Esta funcionalidad vuelve el sistema a cero y es usadada solo a fines de prueba.
```
Request
POST /reset 

Response
200
{ "resultado": "ok"}
```
