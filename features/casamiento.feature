#language: es
Característica: Presupuestación de casamiento

Antecedentes:
  Dado que mi cuit personal es "20445556667"

  Escenario: c1 - Casamiento discreto con menu mixto
    Dado un evento "casamiento" con servicio "normal"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 10 comensales con menú "mixto"
    Cuando se prepuesta
    Entonces el importe resultante es 3050.0
    #COSTO_BASE (150) * CANTIDAD_DE_COMENZALES(10) + CANTIDAD_DE_MOZOS(1) * COSTO_MOZO (1000) + MENU (55) * CANTIDAD_DE_COMENZALES(10) + COSTO_SALON (si es contratado)

  Escenario: c2 - Casamiento ampuloso con tres menus
    Dado un evento "casamiento" con servicio "premium"
    Y que está programado para el "2021-05-11" que es un día hábil
    Y que tendrá 50 comensales con menú "mixto"
    Y que tendrá 50 comensales con menú "veggie"
    Y que tendrá 50 comensales con menú "carnie"
    Cuando se prepuesta
    Entonces el importe resultante es 15250.0
