#language: es
Característica: Presupuestación libre

Antecedentes:
  Dado que mi cuit personal es "20445556667"

  Escenario: c3 - Casamiento veggie
    Dado un evento "casamiento" con servicio "normal"
    Y que está programado para el "2021-05-07" que es un día hábil
    Y que tendrá 230 comensales con menú "veggie"
    Cuando se prepuesta
    Entonces el importe resultante es 47600.0

  Escenario: e1 - Evento empresarial sin cupo minimo
    Dado un evento "empresarial" con servicio "premium"
    Y que está programado para el "2021-05-09" que es un domingo
    Y que tendrá 4 comensales con menú "veggie"
    Y que tendrá 4 comensales con menú "mixto"
    Y que tendrá 2 comensales con menú "veggie"
    Cuando se prepuesta
    Entonces obtengo un error

